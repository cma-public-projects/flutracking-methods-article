This  `inputs`  folder will be created for all users of the repo, but all new files inside it will be ignored by git. This enables people to put the *very large* FluTracking data files here to read into the code without bloating the git repo irrevocably.

Currently the only tracked files are the age band concordances needed to aggregate the age ranges and coarse grained 2021 estimated residential populations projections by Statistics NZ with 5 year age bands for each DHB [1].

[1] Statistics NZ, Technical notes dhb ethnic projections (2021 update) (2021).