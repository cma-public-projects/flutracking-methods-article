# FluTracking Methods Article

This repository contains R code [and supporting example data] that demonstrates the application of the methodology described in the [methods article](https://doi.org/10.1016/j.mex.2022.101820) available on MethodsX. The Rmarkdown document contained within the `src` folder, `FluTrackingMethods_MethodsX.Rmd`, was used to generate the figures found in **Incidence Classification** (Figure 3) and  **Method Demonstration** (Figures 5-9), as well as Figures 1, 3 and 4 in **Section A and B** of the supplementary material. 

This document can be used with a simplified masked dataset (of the full dataset), contained in the `inputs` folder, to generate the aforementioned figures for a representative survey period and for responses from Auckland Metro DHBs and Wellington DHBs. 

To obtain the full Aotearoa New Zealand dataset for FluTracking please contact the Ministry of Health (NZ) at [nzmoh\_flutracking@health.govt.nz](mailto:nzmoh_flutracking@health.govt.nz).

## Companion App

An interactive app that demonstrates the use of the full dataset and our outputs can be found at: https://emilynz.shinyapps.io/fluTracking-shiny-app/.

## Licensing

Shield: [![CC BY-NC-SA 4.0][cc-by-nc-sa-shield]][cc-by-nc-sa]

This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0
International License][cc-by-nc-sa].

[![CC BY-NC-SA 4.0][cc-by-nc-sa-image]][cc-by-nc-sa]

[cc-by-nc-sa]: http://creativecommons.org/licenses/by-nc-sa/4.0/
[cc-by-nc-sa-image]: https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png
[cc-by-nc-sa-shield]: https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg
